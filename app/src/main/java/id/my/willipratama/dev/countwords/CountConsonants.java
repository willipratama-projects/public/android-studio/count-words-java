package id.my.willipratama.dev.countwords;

public class CountConsonants {
    private int consonants;

    public int getCountConsonants() {
        return consonants;
    }

    public void setCountConsonants(String words) {
        int i;
        this.consonants = 0;

        for (i = 0; i < words.length(); i++) {
            if (words.charAt(i) != 'a' && words.charAt(i) != 'i' && words.charAt(i) != 'u' && words.charAt(i) != 'e' && words.charAt(i) != 'o') {
                consonants++;
            }
        }
    }
}
