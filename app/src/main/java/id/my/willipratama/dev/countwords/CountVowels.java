package id.my.willipratama.dev.countwords;

public class CountVowels {
    private int vowels;

    public int getCountVowels() {
        return vowels;
    }

    public void setCountVowels(String words) {
        int i;
        this.vowels = 0;

        for (i = 0; i < words.length(); i++) {
            if (words.charAt(i) == 'a' || words.charAt(i) == 'i' || words.charAt(i) == 'u' || words.charAt(i) == 'e' || words.charAt(i) == 'o') {
                vowels++;
            }
        }
    }
}
