package id.my.willipratama.dev.countwords;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // inisialisasi variabel ke dalam masing-masing obyek
    private EditText edtWords;
    private Button btnCalculate, btnRefresh;
    private TextView tvVowels, tvConsonants;
    private String words;

    // inisialisasi variabel khusus ketika 'death state' pada activity
    // https://developer.android.com/topic/libraries/architecture/saving-states
    private static final String STATE_WORDS = "state_words";
    private static final String STATE_VOWELS = "state_vowels";
    private static final String STATE_CONSONANTS = "state_consonants";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // sinkronisasi variabel ke dalam masing-masing komponen di layout yang telah dibuat
        edtWords = findViewById(R.id.edt_words);
        btnCalculate = findViewById(R.id.btn_calculate);
        btnRefresh = findViewById(R.id.btn_refresh);
        tvVowels = findViewById(R.id.tv_vowels);
        tvVowels.setText("0");
        tvConsonants = findViewById(R.id.tv_consonants);
        tvConsonants.setText("0");

        // inisialisasi tombol agar menjalankan proses ketika ditekan
        btnCalculate.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);

        // proses logika ketika 'death state' setelah activity dibuat (onCreate) agar data tidak hilang
        if (savedInstanceState != null) {
            edtWords.setText(savedInstanceState.getString(STATE_WORDS));
            tvVowels.setText(savedInstanceState.getString(STATE_VOWELS));
            tvConsonants.setText(savedInstanceState.getString(STATE_CONSONANTS));
        }
    }

    // proses yang dilakukan ketika sudah terjadi save states
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_WORDS, edtWords.getText().toString().trim());
        outState.putString(STATE_VOWELS, tvVowels.getText().toString().trim());
        outState.putString(STATE_CONSONANTS, tvConsonants.getText().toString().trim());
    }

    // proses yang dilakukan ketika ada kejadian suatu komponen ditekan
    @Override
    public void onClick(View view) {
        // proses ketika tombol btn_calculate ditekan
        if (view.getId() == R.id.btn_calculate) {
            words = edtWords.getText().toString().trim();
            // eliminasi spasi dan baris baru di dalam kalimat karena kita tidak boleh mengkategorikannya sebagai huruf konsonan
            words = words.replaceAll(" ", "");
            words = words.replaceAll("[\n\r]", "");
            // inisialisasi variabel untuk digunakan ketika tidak ada teks di dalam variabel words
            boolean isEmptyFields = false;

            // pengecekan ketika variabel words tidak ada isi
            if (TextUtils.isEmpty(words)) {
                // set variabel isEmptyFields ketika variabel words tidak ada isi
                isEmptyFields = true;
                // dan TextInput akan menampilkan pesan error
                edtWords.setError(getResources().getString(R.string.text_empty_error_message));
            }

            // proses yang dilakukan ketika variabel words ada isinya
            if (!isEmptyFields) {
                CountVowels vowels = new CountVowels();
                vowels.setCountVowels(words);
                tvVowels.setText(String.valueOf(vowels.getCountVowels()));

                CountConsonants consonants = new CountConsonants();
                consonants.setCountConsonants(words);
                tvConsonants.setText(String.valueOf(consonants.getCountConsonants()));
            }
        }

        // proses ketika tombol btn_refresh ditekan, akan mengembalikan tampilan komponen teks ke awal
        if (view.getId() == R.id.btn_refresh) {
            edtWords.setText("");
            tvVowels.setText("0");
            tvConsonants.setText("0");
        }
    }
}